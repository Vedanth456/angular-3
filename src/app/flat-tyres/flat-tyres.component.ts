import { Component, OnInit } from '@angular/core';
import { CustService } from '../cust.service';
import { Router } from '@angular/router';

@Component({
  selector: 'app-flat-tyres',
  templateUrl: './flat-tyres.component.html',
  styleUrls: ['./flat-tyres.component.css']
})
export class FlatTyresComponent implements OnInit{

  cartItems: any;
  flat:any;

  constructor(private service:CustService,private router:Router){

    this.cartItems = [];

    this.flat = [
      {id: 101, name:'FlatTyres', price:1.33, imagePath:'assets/Images/flatTyres.png', description:'Unleaded Petrol , Qualified Agents , 24/7 On Spot Service, Live Support'},
      {id: 102, name:'Towing',   price:1.42, imagePath:'assets/Images/towing.png', description:'Unleaded Petrol , Qualified Agents , 24/7 On Spot Service, Live Support'},
       {id: 103, name:'BatteryJumpStart',  price:1.51, imagePath:'assets/Images/Jump.png', description:'Unleaded Petrol , Qualified Agents , 24/7 On Spot Service, Live Support'}
    ];



  }
  ngOnInit(){
   
  }

  addToCart(fueld: any) {
    this.service.addToCart(fueld);
    // this.cartItems.push(fueld);
    // localStorage.setItem('cartItems', JSON.stringify(this.cartItems));
  }



  about(){
    this.router.navigate(['aboutcust']);
  }

  services(){
    this.router.navigate(['aboutcust']);

  }

  logout(){
    this.router.navigate(['logout']);

  }
  
  cart(){
    this.router.navigate(['cart']);
  }
  contact(){
    this.router.navigate(['logout']);
  }

  fuelDelivery(){
    this.router.navigate(['fueldel']);

  }

  flatTyres(){
    this.router.navigate(['flattyres']);
  }


}
