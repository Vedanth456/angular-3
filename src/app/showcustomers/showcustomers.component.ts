import { Component, OnInit } from '@angular/core';
import { CustService } from '../cust.service';

declare var jQuery: any;
@Component({
  selector: 'app-showcustomers',
  templateUrl: './showcustomers.component.html',
  styleUrls: ['./showcustomers.component.css']
})
export class ShowcustomersComponent implements OnInit {
  customers: any;
  editCust: any;
  countries:any;

  //Date Format: MM-dd-yyyy

  constructor(private service:CustService) {

    // this.customers = [
    //   { custId:101, name:'Harsha',  gender:'Male',  country:'INDIA', emailId:'harsha@gmail.com', password:'123'},
    //   { custId:102, name:'Pasha',  gender:'Male',   country:'INDIA', emailId:'pasha@gmail.com',  password:'123'},
    //   { custId:103, name:'Indira',   gender:'Female',  country:'INDIA', emailId:'indira@gmail.com', password:'123'},
    //   { custId:104, name:'Venkat',  gender:'Male',    country:'INDIA', emailId:'venkat@gmail.com', password:'123'},
    //   { custId:105, name:'Vikas', gender:'Male',    country:'INDIA', emailId:'vikas@gmail.com',  password:'123'},
    //   { custId:106, name:'Gopi',   gender:'Male',   country:'INDIA', emailId:'gopi@gmail.com',   password:'123'}
    // ];
    this.editCust = {custId:'', name:'', gender:'', mobileNo:'', emailId:'', password:''};

  }
  ngOnInit() {
    this.service.getAllCustomers().subscribe((data: any) => {
      this.customers = data;
      console.log(data);
    });
    this.service.getAllCountries().subscribe((data: any) => {this.countries = data;});
   
  }



  editCustomer(cust: any) {
    this.editCust = cust;
    console.log(cust);

    jQuery('#editCustomer').modal('show');
  }

  updateCust() {
    console.log(this.editCust);
    this.service.updateCustomer(this.editCust).subscribe((data: any) => {console.log(data);});
  }

  deleteCustomer(cust: any) {
    this.service.deleteCustomer(cust.custId).subscribe((data: any) => {console.log(data);});

    const i = this.customers.findIndex((customer: any) => {
      return customer.custId === cust.custId;
    });
    this.customers.splice(i, 1);

  }

}
