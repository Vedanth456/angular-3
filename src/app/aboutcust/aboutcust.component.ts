import { Component, OnInit } from '@angular/core';
import { CustService } from '../cust.service';
import { Router } from '@angular/router';

@Component({
  selector: 'app-aboutcust',
  templateUrl: './aboutcust.component.html',
  styleUrls: ['./aboutcust.component.css']
})
export class AboutcustComponent implements OnInit {
 
  cartItems:any;
  
constructor(private service:CustService,private router:Router){

}
  ngOnInit() {
    this.cartItems = this.service.cartItems;
  }
about(){
  this.router.navigate(['aboutcust']);
}
login(){
  this.router.navigate(['login']);
}
signUp(){
  this.router.navigate(['register']);
}
contact(){
  this.router.navigate(['contact']);
}

services(){
  this.router.navigate(['services']);

}

logout(){
  this.router.navigate(['logout']);

}

fuelDelivery(){
  this.router.navigate(['fueldel']);

}

home(){
  this.router.navigate(['customer']);
}

customer(){
  this.router.navigate(['customer']);
}
cart(){
  this.router.navigate(['cart']);
}
flatTyres(){
  this.router.navigate(['flatTyres']);
}


}
