import { Injectable } from '@angular/core';

// @ts-ignore
import * as bcrypt from 'bcryptjs';


@Injectable({
  providedIn: 'root'
})
export class EncoService {
  // private key = CryptoJS.enc.Utf8.parse(environment.EncryptKey);
  // private iv = CryptoJS.enc.Utf8.parse(environment.EncryptIV);
  

  constructor() { }

  // encryptPassword(password: string): string {
  //   const hashedPassword = CryptoJS.SHA512(password).toString();
  //   return hashedPassword;
  // }
 //************restore */
  encryptPassword(password: string): string {
    const salt = bcrypt.genSaltSync(10); // Generate a salt with 10 rounds
    const hashedPassword = bcrypt.hashSync(password, salt);
    return hashedPassword;
  }

  // encryptPassword(password: string): string {
  //   const saltRounds = 10;
  //   const salt = bcrypt.genSaltSync(saltRounds);
  //   const hashedPassword = bcrypt.hashSync(password, salt);
  //   return hashedPassword;
  // }
  // convertText(conversion:string) {  
  //   if (conversion=="encrypt") {  
  //     this.conversionEncryptOutput = CryptoJS.AES.encrypt(this.plainText.trim(), this.encPassword.trim()).toString();  
  //   }  
  //   else {  
  //     this.conversionDecryptOutput = CryptoJS.AES.decrypt(this.encryptText.trim(), this.decPassword.trim()).toString(CryptoJS.enc.Utf8);  
     
  // }  
// }  


}