import { Component, OnInit } from '@angular/core';
import { CustService } from '../cust.service';
import { Router } from '@angular/router';

@Component({
  selector: 'app-customer',
  templateUrl: './customer.component.html',
  styleUrls: ['./customer.component.css']
})
export class CustomerComponent implements OnInit {
  
  cartItems:any;

  constructor(private service:CustService,private router:Router){

  }
  
  ngOnInit() {
    this.cartItems = this.service.cartItems;  
  }
  
  home(){
    this.router.navigate(['customer']);
  }

  about(){
    this.router.navigate(['aboutcust']);
  }

  services(){
    this.router.navigate(['services']);

  }

  logout(){
    this.router.navigate(['logout']);

  }

  contact(){
    this.router.navigate(['contact']);
  }

  fuelDelivery(){
    this.router.navigate(['fueldel']);

  }

  customer(){
    this.router.navigate(['gahg']);
  }
  cart(){
    this.router.navigate(['cart']);
  }
  flatTyres(){
    this.router.navigate(['flatTyres']);
  }


}
