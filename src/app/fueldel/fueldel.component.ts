import { Component, OnInit } from '@angular/core';
import { CustService } from '../cust.service';
import { Router } from '@angular/router';

@Component({
  selector: 'app-fueldel',
  templateUrl: './fueldel.component.html',
  styleUrls: ['./fueldel.component.css']
})
export class FueldelComponent implements OnInit{
  cartItems: any;
  fueldel:any;

  constructor(private service:CustService,private router:Router){

    this.cartItems = [];

    this.fueldel = [
      {id: 1001, name:'2L', price:1.33, imagePath:'assets/Images/2L can.jpg', description:'Unleaded Petrol , Qualified Agents , 24/7 On Spot Service, Live Support'},
      {id: 1002, name:'5L',   price:1.42, imagePath:'assets/Images/5L can.jpg', description:'Unleaded Petrol , Qualified Agents , 24/7 On Spot Service, Live Support'},
       {id: 1003, name:'10L',    price:1.51, imagePath:'assets/Images/10L can.jpg', description:'Unleaded Petrol , Qualified Agents , 24/7 On Spot Service, Live Support'}
    ];



  }
  ngOnInit(){
   
  }

  addToCart(fueld: any) {
    this.service.addToCart(fueld);
    // this.cartItems.push(fueld);
    // localStorage.setItem('cartItems', JSON.stringify(this.cartItems));
  }



  about(){
    this.router.navigate(['aboutcust']);
  }

  services(){
    this.router.navigate(['aboutcust']);

  }

  logout(){
    this.router.navigate(['logout']);

  }
  
  cart(){
    this.router.navigate(['cart']);
  }
  contact(){
    this.router.navigate(['logout']);
  }

  fuelDelivery(){
    this.router.navigate(['fueldel']);

  }

  flatTyres(){
    this.router.navigate(['flatTyres']);
  }

}
