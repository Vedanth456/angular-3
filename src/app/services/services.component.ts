import { Component, OnInit } from '@angular/core';
import { Router } from '@angular/router';
import { CustService } from '../cust.service';

@Component({
  selector: 'app-services',
  templateUrl: './services.component.html',
  styleUrls: ['./services.component.css']
})
export class ServicesComponent implements OnInit{

  cartItems:any;

  constructor(private service:CustService,private router:Router){

  }
  
  ngOnInit() {
    this.cartItems = this.service.cartItems;  
  }
  
  home(){
    this.router.navigate(['customer']);
  }

  about(){
    this.router.navigate(['aboutcust']);
  }

  services(){
    this.router.navigate(['services']);

  }

  logout(){
    this.router.navigate(['logout']);

  }

  contact(){
    this.router.navigate(['contact']);
  }

  fuelDelivery(){
    this.router.navigate(['fueldel']);

  }

  customer(){
    this.router.navigate(['gahg']);
  }
  cart(){
    this.router.navigate(['cart']);
  }
  flatTyres(){
    this.router.navigate(['flatTyres']);
  }


}
