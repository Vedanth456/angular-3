import { Component, OnInit } from '@angular/core';
import { CustService } from '../cust.service';
import { Router } from '@angular/router';
import { PaymentService } from '../payment.service';

@Component({
  selector: 'app-cart',
  templateUrl: './cart.component.html',
  styleUrls: ['./cart.component.css']
})
export class CartComponent implements OnInit {

  cartItems: any;
  cartData: any;
  total: number;

  constructor(private service:CustService,private router:Router,private payment:PaymentService) {
    this.cartItems = [];
    this.total = 0;
  }

  ngOnInit() {
    // this.cartData = localStorage.getItem('cartItems');
    // this.cartItems = JSON.parse(this.cartData);

    this.cartItems = this.service.cartItems;

    this.cartItems.forEach((fueldel: any) => {
      this.total = this.total + fueldel.price;      
    });
  }

  purchase(){
    this.payment.totalAmount = this.total;
    this.router.navigate(['purchase']);

  }

}