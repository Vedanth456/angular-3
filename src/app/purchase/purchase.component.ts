
import { Component, ElementRef, NgZone, OnInit, ViewChild } from '@angular/core';
import {  Router } from '@angular/router';
import { PaymentService } from '../payment.service';
// declare var Razorpay: any;

@Component({
  selector: 'app-purchase',
  templateUrl: './purchase.component.html',
  styleUrls: ['./purchase.component.css']
})
export class PurchaseComponent implements OnInit {



  amount = 0;

  @ViewChild('paymentRef', {static: true}) paymentRef!: ElementRef;

  constructor(private router: Router, private payment: PaymentService,private ngZone: NgZone) { }
   
  ngOnInit(): void {
    this.amount = this.payment.totalAmount;
    console.log(window.paypal);
    window.paypal.Buttons(
      {
        style: {
          layout: 'horizontal',
          color: 'blue',
          shape: 'rect',
          label: 'paypal',
        },
 
        createOrder: (data: any, actions: any) => {
          return actions.order.create({
            purchase_units: [
              {
                amount: {
                  value: this.amount.toString(),
                  currency_code: 'USD'
                }
              }
            ]
          });
        },   
        onApprove: (data: any, actions: any) => {
          return actions.order.capture().then((details: any) => {
            // if (details.status === 'COMPLETED') {
            //   this.payment.transactionID = details.id;
            //   this.router.navigate(['confirm']);  
            // }
            this.payment.transactionID = details.id;
          //  this.click();
          console.log(details);

          this.ngZone.run(() => {
            this.router.navigate(['/confirm']); // Navigate to the customer page
          });
            // this.router.navigate(['confirm']);  
            
          }).catch((error: any) => {
            console.log('Error during payment capture:', error);
          });
        },
        onError: (error: any) => {
          console.log(error);
        }
      }
    ).render(this.paymentRef.nativeElement);
  }

  cancel() {
    this.router.navigate(['cart']);
  }

  click(){
    this.router.navigate(['con']);
  }


}

