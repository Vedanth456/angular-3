import { NgModule } from '@angular/core';
import { RouterModule, Routes } from '@angular/router';
import { LoginComponent } from './login/login.component';
import { RegisterComponent } from './register/register.component';
import { ShowcustomersComponent } from './showcustomers/showcustomers.component';
import { GetcustbyidComponent } from './getcustbyid/getcustbyid.component';
import { AuthGuard } from './auth.guard';
import { HeaderComponent } from './header/header.component';
// import { LogoutComponent } from './logout/logout.component';
import { PetrolComponent } from './petrol/petrol.component';
import { LogoutComponent } from './logout/logout.component';
import { AboutComponent } from './about/about.component';
import { WelcomeComponent } from './welcome/welcome.component';
import { AdminComponent } from './admin/admin.component';
import { CustomerComponent } from './customer/customer.component';
import { FueldelComponent } from './fueldel/fueldel.component';
import { CartComponent } from './cart/cart.component';
import { PurchaseComponent } from './purchase/purchase.component';
import { ConfirmationComponent } from './confirmation/confirmation.component';
import { AboutcustComponent } from './aboutcust/aboutcust.component';
import { ContactComponent } from './contact/contact.component';
import { MobileNumberComponent } from './mobile-number/mobile-number.component';
import { SetpasswordComponent } from './setpassword/setpassword.component';
import { ForgotComponent } from './forgot/forgot.component';
import { OtpComponent } from './otp/otp.component';
import { FlatTyresComponent } from './flat-tyres/flat-tyres.component';
import { Contact2Component } from './contact2/contact2.component';
    
const routes: Routes = [
  // {path:'', component:LoginComponent},
  // {path:'', component:PetrolComponent},
  {path:'', component:WelcomeComponent},
  {path:'login', component:LoginComponent},
  {path:'register', component:RegisterComponent},
  {path:'showcust', canActivate:[AuthGuard],component:ShowcustomersComponent},
  {path:'custbyid',canActivate:[AuthGuard], component:GetcustbyidComponent},
  {path:'logout', component:LogoutComponent},
  {path:'header',component:HeaderComponent},
  {path:'about',component:AboutComponent},
  {path:'admin',component:AdminComponent},
  {path:'customer',component:CustomerComponent},
  {path:'fueldel',component:FueldelComponent},
  {path:'cart',component:CartComponent},
  {path:'purchase',component:PurchaseComponent},
  {path:'confirm',component:ConfirmationComponent},
  {path:'aboutcust',component:AboutcustComponent},
  {path:'contact',component:ContactComponent},
  {path:'mobile',component:MobileNumberComponent},
  {path:'setpassword',component:SetpasswordComponent},
  {path:'forgot',component:ForgotComponent},
  {path:'otp',component:OtpComponent},
  {path:'flatTyres',component:FlatTyresComponent},
  {path:'contact2',component:Contact2Component}

];

@NgModule({
  imports: [RouterModule.forRoot(routes)],
  exports: [RouterModule]
})
export class AppRoutingModule { }
