import { Component } from '@angular/core';
import { Router } from '@angular/router';
import { CustService } from '../cust.service';

@Component({
  selector: 'app-contact2',
  templateUrl: './contact2.component.html',
  styleUrls: ['./contact2.component.css']
})
export class Contact2Component {
  
constructor(private service:CustService,private router:Router){

}

about(){
  this.router.navigate(['about']);
}
login(){
  this.router.navigate(['login']);
}
signUp(){
  this.router.navigate(['register']);
}
contact(){
  this.router.navigate(['contact2']);
}

}
