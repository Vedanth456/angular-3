import { Component } from '@angular/core';
import { CustService } from '../cust.service';
import { Router } from '@angular/router';

@Component({
  selector: 'app-welcome',
  templateUrl: './welcome.component.html',
  styleUrls: ['./welcome.component.css']
})
export class WelcomeComponent {


constructor(private service:CustService,private router:Router){

}
home(){
  this.router.navigate(['welcome']);
}
about(){
  this.router.navigate(['about']);
}
login(){
  this.router.navigate(['login']);
}
signUp(){
  this.router.navigate(['register']);
}
contact(){
  this.router.navigate(['contact2']);
}

}
