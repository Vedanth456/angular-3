import { Component } from '@angular/core';
import { Router } from '@angular/router';
import { CustService } from '../cust.service';

@Component({
  selector: 'app-sample',
  templateUrl: './sample.component.html',
  styleUrls: ['./sample.component.css']
})
export class SampleComponent {
  
constructor(private service:CustService,private router:Router){

}

about(){
  this.router.navigate(['about']);
}
login(){
  this.router.navigate(['login']);
}
signUp(){
  this.router.navigate(['register']);
}
contact(){
  this.router.navigate(['contact2']);
}

}
