import { NgModule } from '@angular/core';
import { BrowserModule } from '@angular/platform-browser';

import { NgxCaptchaModule } from 'ngx-captcha';
import { AppRoutingModule } from './app-routing.module';
import { AppComponent } from './app.component';
import { LoginComponent } from './login/login.component';
import { FormsModule } from '@angular/forms';
import { ShowcustomersComponent } from './showcustomers/showcustomers.component';
import { GenderPipe } from './gender.pipe';
import { RegisterComponent } from './register/register.component';
import { GetcustbyidComponent } from './getcustbyid/getcustbyid.component';
import { HeaderComponent } from './header/header.component';
// import { ROUTES, RouterModule } from '@angular/router';
import { Routes, RouterModule } from '@angular/router';
import { HttpClientModule } from '@angular/common/http';
import { LogoutComponent } from './logout/logout.component';
import { ToastrModule } from 'ngx-toastr';
import { BrowserAnimationsModule } from '@angular/platform-browser/animations';
// import { NgxCaptchaModule } from 'ngx-captcha';
import { PetrolComponent } from './petrol/petrol.component';
import { ReactiveFormsModule } from '@angular/forms';

import { SocialLoginModule, SocialAuthServiceConfig, GoogleSigninButtonModule } from '@abacritt/angularx-social-login';
import {
  GoogleLoginProvider
} from '@abacritt/angularx-social-login';
import { AdminComponent } from './admin/admin.component';
import { AboutComponent } from './about/about.component';
import { WelcomeComponent } from './welcome/welcome.component';
import { FooterComponent } from './footer/footer.component';
import { CustomerComponent } from './customer/customer.component';
import { FueldelComponent } from './fueldel/fueldel.component';
import { CartComponent } from './cart/cart.component';
import { PurchaseComponent } from './purchase/purchase.component';
import { ConfirmationComponent } from './confirmation/confirmation.component';
import { AboutcustComponent } from './aboutcust/aboutcust.component';
import { ContactComponent } from './contact/contact.component';
import { ForgotComponent } from './forgot/forgot.component';
import { MobileNumberComponent } from './mobile-number/mobile-number.component';
import { OtpComponent } from './otp/otp.component';
import { SetpasswordComponent } from './setpassword/setpassword.component';
import { FlatTyresComponent } from './flat-tyres/flat-tyres.component';
import { Contact2Component } from './contact2/contact2.component';
import { SampleComponent } from './sample/sample.component';
import { ServicesComponent } from './services/services.component';


  

@NgModule({
  declarations: [
    AppComponent,
    LoginComponent,
    ShowcustomersComponent,
    GenderPipe,
    RegisterComponent,
    GetcustbyidComponent,
    HeaderComponent,
    LogoutComponent,
    PetrolComponent,
    AdminComponent,
    AboutComponent,
    WelcomeComponent,
    FooterComponent,
    CustomerComponent,
    FueldelComponent,
    CartComponent,
    PurchaseComponent,
    ConfirmationComponent,
    AboutcustComponent,
    ContactComponent,
    ForgotComponent,
    MobileNumberComponent,
    OtpComponent,
    SetpasswordComponent,
    FlatTyresComponent,
    Contact2Component,
    SampleComponent,
    ServicesComponent,
  ],
  imports: [
    BrowserModule,
    AppRoutingModule,
    FormsModule,
    RouterModule,
    HttpClientModule,
    ToastrModule.forRoot(),
    ToastrModule,
    BrowserAnimationsModule,
    ReactiveFormsModule,
    NgxCaptchaModule,
    SocialLoginModule,
    GoogleSigninButtonModule

    
    // ReCaptchaModule,
    // ReCaptcha

  ],
  
  providers: [

    {
      provide: 'SocialAuthServiceConfig',
      useValue: {
        autoLogin: false,
        providers: [
          {
            id: GoogleLoginProvider.PROVIDER_ID,
            provider: new GoogleLoginProvider(
              '349755697035-p4ubvrh21je60hgp8214k6pgn4pd8nr6.apps.googleusercontent.com'
            )
          }
          // ,
          // {
          //   id: FacebookLoginProvider.PROVIDER_ID,
          //   provider: new FacebookLoginProvider('clientId')
          // }
        ],
        onError: (err) => {
          console.error(err);
        }
      } as SocialAuthServiceConfig,
    }
  ],
  bootstrap: [AppComponent]
})
export class AppModule { }
