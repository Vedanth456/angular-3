import { Component } from '@angular/core';
import { Router } from '@angular/router';
import { CustService } from '../cust.service';

@Component({
  selector: 'app-mobile-number',
  templateUrl: './mobile-number.component.html',
  styleUrls: ['./mobile-number.component.css']
})
export class MobileNumberComponent {
  mobileNumber: any;



  constructor(private router: Router, private service: CustService) {}

  ngOnInit() {
    
  }

  // checking for valid mobile number and navigating to the next page if its correct
  mob(mobForm: any) {
    console.log(mobForm);
    this.service.setMobile(mobForm.mobileNumber);
    // sessionStorage.setItem(this.mobileNumber,mobForm.mobileNumber); 
    this.service.getOtp(mobForm.mobileNumber).subscribe((data1: any) => {
      console.log("mdkd");
      this.service.setData(data1);
      console.log(data1);
      if (data1 != 0) {
        this.router.navigate(['otp']);
      } else {
        let msz = <HTMLElement>document.getElementById('popmsz');
        msz.innerHTML = 'Invalid Mobile Number';
      }
    });
  }

}
