import { Component } from '@angular/core';
import { Router } from '@angular/router';
import { CustService } from '../cust.service';
import { ToastrService } from 'ngx-toastr';

@Component({
  selector: 'app-setpassword',
  templateUrl: './setpassword.component.html',
  styleUrls: ['./setpassword.component.css']
})
export class SetpasswordComponent {

  setPass: any;

  constructor(private router: Router, private service: CustService,private toastr:ToastrService) {
    this.setPass = {
      mobileNumber: '',
      password: '',
    };   
  }

  ngOnInit(): void {
    throw new Error('Method not implemented.');
  }


  pass() {
    console.log('inside pass method');
    console.log(this.setPass);
    this.setPass.mobileNumber = this.service.getMobile();
    console.log(this.setPass.mobileNumber);
    this.service.updatePass(this.setPass).subscribe(
      (data: any) => {
        if (data !== 0) {
          this.toastr.success(" Password Changed Successfully");
          this.router.navigate(['login']);
        } else {  
          const msz = document.getElementById('popmsz');
          if (msz) {
            msz.innerHTML = 'Failed to change password';
          }
        }
      },  
      (error: any) => {
        console.log('Error occurred while updating password:', error);
      }
    );
  }


}
