import { Component, OnInit } from '@angular/core';
import { CustService } from '../cust.service';
// import { ToastrService } from 'ngx-toastr';
import { Router } from '@angular/router';
import { FormBuilder, FormGroup, FormsModule, Validators } from '@angular/forms';
import { EncoService } from '../enco.service';
import { ToastrService } from 'ngx-toastr';


@Component({
  selector: 'app-register',
  templateUrl: './register.component.html',
  styleUrls: ['./register.component.css']
})
export class RegisterComponent implements OnInit {
  protected aFormGroup: FormGroup | undefined;
  customers: any;
  customer:any;
  countries:any;
  
  regForm: FormGroup | undefined;
  otpSent: boolean = false;
  otpVerified: boolean = false;

  // registrationForm: FormGroup;
  // passwordMatchValidator: any;

  //Date Format: MM-dd-yyyy  private toastr: ToastrService,

  constructor(private service :CustService,private enco:EncoService, private toastr: ToastrService, private router:Router,private formBuilder: FormBuilder) {

    // this.customers = [
    //   { custId:101, name:'Harsha',  gender:'Male',  country:'INDIA', emailId:'harsha@gmail.com', password:'123'},
    //   { custId:102, name:'Pasha',  gender:'Male',   country:'INDIA', emailId:'pasha@gmail.com',  password:'123'},
    //   { custId:103, name:'Indira',   gender:'Female',  country:'INDIA', emailId:'indira@gmail.com', password:'123'},
    //   { custId:104, name:'Venkat',  gender:'Male',    country:'INDIA', emailId:'venkat@gmail.com', password:'123'},
    //   { custId:105, name:'Vikas', gender:'Male',    country:'INDIA', emailId:'vikas@gmail.com',  password:'123'},
    //   { custId:106, name:'Gopi',   gender:'Male',   country:'INDIA', emailId:'gopi@gmail.com',   password:'123'}
    // ];

    this.customer = {custId:'', name:'', gender:'', mobileNo:'', emailId:'', password:''};
    

  }
 

  
  ngOnInit(){
    this.aFormGroup = this.formBuilder.group({
      recaptcha: ['', Validators.required]
      
    }),
    // this.regForm = this.formBuilder.group({
    //   gender: ['', Validators.required]
    // });
    this.regForm = this.formBuilder.group({
      gender: ['', Validators.required],
      mobileNo: ['', Validators.required],
      otp: ['', Validators.required]
    });
  }

  siteKey="6LeqeDUmAAAAAENMi2oBcLXSgMXxIwAMzbnLMqPg";

  register(regForm: any) {
    console.log(regForm);
    // this.passwordMatchValidator(regForm); 

    this.customer.custId = regForm.custId;
    this.customer.name = regForm.name;
    
    this.customer.gender = regForm.gender;
    
    this.customer.mobileNo = regForm.mobileNo;
    this.customer.emailId = regForm.emailId;
    this.customer.password = regForm.password; 

    // this.customers.push(regForm);
    console.log("entidho");

    const plainPassword = regForm.password;
    // const hashedPassword = this.enco.encryptPassword(regForm.password); // Hash the password with a salt of 10 rounds
    // this.customer.password = hashedPassword;

    

    const hashedPassword = this.enco.encryptPassword(regForm.password) ;
    this.customer.password = hashedPassword;
    
    console.log(this.customers);
    // console.log('Registering customer:', this.customer);
    
    this.service.registerCustomer(this.customer).subscribe((result: any) => {
      console.log(result);
      if(result!=null){
        // this.sendRegistrationEmail(regForm.emailId);
        this.toastr.success('Customer Registered Successfully', 'Success');
        this.router.navigate(['login']);  
        
      }else {
        this.toastr.error('Failed to register customer,EmailId already exists', 'Error');
      }
    },

    (error: any) => {
      console.log(error);
      this.toastr.error('Failed to register customer,EmailId already exists', 'Error');
    }
    );
  }

  //*********** *
login(){
  this.router.navigate(['login']);
}
 
about(){
  this.router.navigate(['about']);
}

signUp(){
  this.router.navigate(['register']);
}
contact(){
  this.router.navigate(['contact2']);
}

  
  

}