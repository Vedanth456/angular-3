
import { Component, OnInit } from '@angular/core';
import { CustService } from '../cust.service';
import { Router } from '@angular/router';
import { ToastrService } from 'ngx-toastr';

import { FormBuilder, FormGroup, Validators } from '@angular/forms';
import { EncoService } from '../enco.service';
// import { ToastrService } from 'ngx-toastr';
// @ts-ignore
import * as bcrypt from 'bcryptjs';  
import { SocialAuthService, SocialUser } from '@abacritt/angularx-social-login';



declare const gapi: any;

@Component({
  selector: 'app-login',
  templateUrl: './login.component.html',
  styleUrls: ['./login.component.css']
})
export class LoginComponent implements OnInit {
  
  customers:any;
  customer:any;
  emailId: any;
  password: any;
  loggedIn: any;
  // passwordForm: FormGroup;
  // passwordForm: FormGroup;

  //Implementing Dependency Injection for EmpService , private toastr:ToastrService,
  constructor(private service: CustService,private authService: SocialAuthService,private enco:EncoService,  private toastr:ToastrService, private router: Router,private encr : EncoService ,private formBuilder: FormBuilder) {

    this.customers = [
      { custId:101, name:'Harsha',  gender:'Male',  country:'INDIA', emailId:'harsha@gmail.com', password:'123'},
      { custId:102, name:'Pasha',  gender:'Male',   country:'INDIA', emailId:'pasha@gmail.com',  password:'123'},
      { custId:103, name:'Indira',   gender:'Female',  country:'INDIA', emailId:'indira@gmail.com', password:'123'},
      { custId:104, name:'Venkat',  gender:'Male',    country:'INDIA', emailId:'venkat@gmail.com', password:'123'},
      { custId:105, name:'Vikas', gender:'Male',    country:'INDIA', emailId:'vikas@gmail.com',  password:'123'},
      { custId:106, name:'Gopi',   gender:'Male',   country:'INDIA', emailId:'gopi@gmail.com',   password:'123'}

      

    ];

    // this.passwordForm = this.formBuilder.group({
    //   password: ['', [Validators.required, Validators.minLength(8), Validators.pattern(/^(?=.[a-z])(?=.[A-Z])(?=.\d)(?=.[@$!%?&])[A-Za-z\d@$!%?&]{8,}$/)]],

    //   emailId: ['', [Validators.required, Validators.email]]
    // });

  }

  ngOnInit(){


    this.authService.authState.subscribe((user: SocialUser | null) => {
      if (user) {
        // Check if the user's email exists in your customer list
        const customer = this.service.getCustomerByEmail(user.email);

        if (customer!=null) {   

          // this.toastr.success(" Welcome to CUSTOMER Home Page");
          this.service.setUserLoggedIn();

          this.router.navigate(['customer']);
        } else {
          // alert('Invalid Credentials');
          this.toastr.error('Invalid Credentials');
        }
      }
    });

    
    // this.authService.authState.subscribe((customer) => {
    //   this.customer = customer;
    //   this.loggedIn = (customer != null);
    // });
    // this.loadGoogleSignInScript();
  }

  submit() {
        if (this.emailId === 'HR' && this.password === 'HR') {
          alert('Welcome to HR Home Page');
    
          console.log('EmailID: ' + this.emailId);
          console.log('Password: ' + this.password);
    
        } else {
          alert('Invalid Credentials');
          console.log('Invalid Credentials');
        }
      }

  async loginSubmit(loginForm: any) { 

    console.log(loginForm);

    
    if (loginForm.emailId === 'ADMIN' && loginForm.password === 'ADMIN') {
      // alert("Welcome to ADMIN Home Page");
     
     
      this.toastr.success(" Welcome to ADMIN Home Page");
      
      this.service.setUserLoggedIn();
      this.router.navigate(['admin']);


    } else {
      this.service.getCustomerByEmail(loginForm.emailId).subscribe(async(customer: any) => {

      // const customer = await this.service.getCustomerByEmail(loginForm.emailId);
      
      if (customer!=null) {     
         const isPasswordValid = await bcrypt.compare(loginForm.password,customer.password);
         console.log(isPasswordValid);
         if (isPasswordValid) {
          console.log(customer.password);

      // await this.service.custLogin({ emailId, password: encryptedPassword }).then((cust: any) => {
        
      //restore
      console.log("yyyyy");
        const hashedPassword = this.enco.encryptPassword(loginForm.password); // Hash the password with a salt of 10 rounds
        console.log(hashedPassword);
        loginForm.password = hashedPassword;
        console.log(loginForm.password);
        console.log(loginForm);

      this.toastr.success('Welcome to CUSTOMER Home Page', '');
      // alert('Customers page');
      this.service.setUserLoggedIn();
      this.router.navigate(['customer']);  
    } else {
      // alert('Invalid Credentials');
      this.toastr.error('Inavlid Credentials');
    }
  } else {
    // alert('Invalid Credentials');
    this.toastr.error('Inavlid Credentials');
  }
});

      await this.service.custLogin(loginForm).then((cust: any) => {
        this.customer = cust;
        console.log(cust);
      });
      console.log(this.customer);

      //restore************
      // if (this.customer != null) {
      //   this.service.setUserLoggedIn();
      //   this.router.navigate(['showcust']);
      // } else {
      //   alert('Invalid Credentials');
      // }
      // alert('Invalid Credentials');hjrcgwh
    }
  
  }

  // loadGoogleSignInScript() {
  //   const script = document.createElement('script');
  //   script.src = 'https://apis.google.com/js/platform.js';
  //   script.onload = () => {
  //     gapi.load('auth2', () => {   
  //       gapi.auth2.init({
  //         client_id: '697860711869-9ilhqp67maggu48j7ca3gn6j7lcullqc.apps.googleusercontent.com',
  //         scope: 'email' 
  //       });
  //     });
  //   };
  //   document.body.appendChild(script);
  // }

  // onGoogleSignIn() {
  //   const auth2 = gapi.auth2.getAuthInstance();
  //   auth2.signIn().then((googleUser: any) => {
  //     const profile = googleUser.getBasicProfile();
  //     const email = profile.getEmail();
  //     this.service.setUserLoggedIn();
  //       this.router.navigate(['showcust']);
  //     // Check if the email exists in your customer list
  //     const customer = this.service.getCustomerByEmail(email);
  //     if (customer!=null) {
  //       this.service.setUserLoggedIn();
  //       this.router.navigate(['showcust']);
  //     } else {
  //       alert('Invalid Credentials');
  //       auth2.signOut();
  //     }
  //   });      
  // }

  onGoogleSignIn(){
    this.authService.signIn('google');
    
  }
  register(){
    this.router.navigate(['register']);
  }

  forgot(){
    this.router.navigate(['forgot']);
  }

  
about(){
  this.router.navigate(['about']);
}
login(){
  this.router.navigate(['login']);
}
signUp(){
  this.router.navigate(['register']);
}
contact(){
  this.router.navigate(['contact2']);
}

}

