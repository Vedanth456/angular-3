import { Component, OnInit } from '@angular/core';
import { CustService } from '../cust.service';
import { Router } from '@angular/router';

@Component({
  selector: 'app-contact',
  templateUrl: './contact.component.html',
  styleUrls: ['./contact.component.css']
})
export class ContactComponent implements OnInit{


  cartItems:any;

  constructor(private service:CustService,private router:Router){

  }
  home(){
    this.router.navigate(['customer']);
  }
  
  ngOnInit() {
    this.cartItems = this.service.cartItems;  
  }

  about(){
    this.router.navigate(['aboutcust']);
  }

  services(){
    this.router.navigate(['aboutcust']);

  }

  logout(){
    this.router.navigate(['logout']);

  }

  contact(){
    this.router.navigate(['contact']);
  }

  fuelDelivery(){
    this.router.navigate(['fueldel']);

  }

  cart(){
    this.router.navigate(['cart']);
  }
  
  flatTyres(){
    this.router.navigate(['logout']);
  }



}
