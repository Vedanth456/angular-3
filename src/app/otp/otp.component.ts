import { Component, ViewChild } from '@angular/core';
import { CustService } from '../cust.service';
import { Router } from '@angular/router';

@Component({
  selector: 'app-otp',
  templateUrl: './otp.component.html',
  styleUrls: ['./otp.component.css']
})
export class OtpComponent {
  @ViewChild('firstInput') firstInput: any;
  @ViewChild('secondInput') secondInput: any;
  @ViewChild('thirdInput') thirdInput: any;
  @ViewChild('fourthInput') fourthInput: any;
  @ViewChild('fifthInput') fifthInput: any;
  @ViewChild('sixthInput') sixthInput: any;
  otpValue: string = '';
  

  constructor(private router: Router, private service: CustService) {}

  ngOnInit(){
  }

  onKeyUp(event: any, nextInput: any) {
    const input = event.target as HTMLInputElement;
    const inputValue = input.value;

    if (inputValue.length === 1 && nextInput) {
      nextInput.focus();
    }
  }

   //confirming password
   confirmpass(otpForm: any) {
    if (otpForm.otpValue == this.service.getData()) {
      this.router.navigate(['setpassword']);
    } else {
      let msz = <HTMLElement>document.getElementById('popmsz');
      msz.innerHTML = 'Invalid OTP';
    }
  }

  
  otp() {
    this.router.navigate(['setpassword']);
   
  }


}
