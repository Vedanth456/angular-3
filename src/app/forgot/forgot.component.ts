import { Component } from '@angular/core';
import { Router } from '@angular/router';

@Component({
  selector: 'app-forgot',
  templateUrl: './forgot.component.html',
  styleUrls: ['./forgot.component.css']
})
export class ForgotComponent {

  constructor(private router: Router) { }

  ngOnInit(){
    
  }

  //navihating to takeemail component
  number() {
    this.router.navigate(['mobile']);

  }

  // navigating to mob component
  email() {
    this.router.navigate(['email']);
  }



}
