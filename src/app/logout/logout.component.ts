import { Component, OnInit } from '@angular/core';
import { Router } from '@angular/router';
import { CustService } from '../cust.service';
import { ToastrService } from 'ngx-toastr';
import { SocialAuthService, SocialUser } from '@abacritt/angularx-social-login';


@Component({
  selector: 'app-logout',
  templateUrl: './logout.component.html',
  styleUrls: ['./logout.component.css']
})
export class LogoutComponent implements OnInit {

  constructor(private service:CustService,private router:Router,private toastr:ToastrService,private authService: SocialAuthService){
    this.toastr.warning("YOU HAVE BEEN LOGGED OUT");
    console.log("edhoo");
    this.service.setUserLoggedOut();

    this.authService.signOut().then(() => {
      // Perform any additional logic after successful sign out
      console.log("edhoo");
      this.service.setUserLoggedOut();
      console.log("inkendhoo");
    this.router.navigate(['login']);
    }).catch((error) => {
      // Handle error if sign out fails
      console.log('Error occurred during sign out:', error);
    });
    console.log("edhoo");
    this.router.navigate(['login']);

  }
  ngOnInit() {
    this.authService.signOut().then(() => {
      // Perform any additional logic after successful sign out
      console.log("edhoo");
      this.service.setUserLoggedOut();
      console.log("inkendhoo");
    this.router.navigate(['login']);
    }).catch((error) => {
      // Handle error if sign out fails
      console.log('Error occurred during sign out:', error);
    });
  }  
  }

