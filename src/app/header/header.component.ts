import { Component, OnInit } from '@angular/core';
import { CustService } from '../cust.service';

@Component({
  selector: 'app-header',
  templateUrl: './header.component.html',
  styleUrls: ['./header.component.css']
})
export class HeaderComponent implements OnInit {
  
 
  loginInfo: any;

  constructor(private service: CustService) {
  }
  
  ngOnInit(){
    // this.cartItems = this.service.cartItems;

    this.service.loginInfoStatus().subscribe((data: any) => {
      this.loginInfo = data;
    });
  }

}