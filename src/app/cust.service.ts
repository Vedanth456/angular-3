import { HttpClient, HttpClientModule } from '@angular/common/http';
import { Injectable } from '@angular/core';
import { Observable, Subject, pipe, throwError } from 'rxjs';
import { catchError } from 'rxjs/operators';

@Injectable({
  providedIn: 'root'
})
export class CustService {
  
 
  otpValue: any;
  mobileNumber: any;

  loginStatus: boolean;
  cartItems: any;
  loginInfo: Subject<any>;

  constructor(private http :HttpClient) { 
    this.loginStatus = false;
    this.cartItems = [];
    this.loginInfo = new Subject();
  }
  
  loginInfoStatus(): any {
    return this.loginInfo.asObservable();
  }



  setMobile(mobValue: any): any {
    // return (this.mobileNumber = mobValue);
    // this.mobileNumber = mobValue;
    // this.mobileNumber.asObservable();

    sessionStorage.setItem(this.mobileNumber,mobValue);
  }


  addToCart(product: any) {
    this.cartItems.push(product);
  }

  getAllCountries(): any {
    return this.http.get('https://restcountries.com/v3.1/all');
  }

  getAllCustomers(): any {
    return this.http.get('http://localhost:8085/getAllCustomers');
  }

  
  updateCustomer(editCust: any): any {
    return this.http.put('updateCustomer', editCust);
  }


  registerCustomer(cust: any): Observable<any> {
      return this.http.post('registerCustomer', cust);
    }

    getCustomerByEmail(email: any) {
      return this.http.get('http://localhost:8085/getCustomerByEmailId/' + email);
    }



  getCustomerById(custId: any): any {
    return this.http.get('http://localhost:8085/getCustomerById/' + custId);
  }

  setUserLoggedIn() {
    this.loginStatus = true;
    this.loginInfo.next(true);
  }

  setUserLoggedOut() {
    this.loginStatus = false;
    this.loginInfo.next(false);
  }

  getLoginStatus(): boolean {
    return this.loginStatus;
  }

  
  deleteCustomer(custId: any) {
    return this.http.delete('deleteCustomer/' + custId);
  }
  
  custLogin(loginForm: any): any {
    return this.http.get('login/' + loginForm.emailId + "/" + loginForm.password).toPromise();
  }


  setData(data: any) {
    this.otpValue = data;
  }

  getOtp(mobileNumber: any): any {
    return this.http.get('/sendsms/' + mobileNumber);
  }


  getMobile(): any {
    return sessionStorage.getItem(this.mobileNumber);
    // return this.mobileNumber;
  }

  updatePass(setForm: any): any {
    return this.http.put('updatePass', setForm);
  }      


  getData(): any {
    return this.otpValue;
  }


}
