import { Component, OnInit } from '@angular/core';
import { CustService } from '../cust.service';

@Component({
  selector: 'app-getcustbyid',
  templateUrl: './getcustbyid.component.html',
  styleUrls: ['./getcustbyid.component.css']
})
export class GetcustbyidComponent implements OnInit {

  cust: any;
  customers: any;

  constructor(private service :CustService) {
    // this.customers = [
    //   { custId:101, name:'Harsha',  gender:'Male',  country:'INDIA', emailId:'harsha@gmail.com', password:'123'},
    //   { custId:102, name:'Pasha',  gender:'Male',   country:'INDIA', emailId:'pasha@gmail.com',  password:'123'},
    //   { custId:103, name:'Indira',   gender:'Female',  country:'INDIA', emailId:'indira@gmail.com', password:'123'},
    //   { custId:104, name:'Venkat',  gender:'Male',    country:'INDIA', emailId:'venkat@gmail.com', password:'123'},
    //   { custId:105, name:'Vikas', gender:'Male',    country:'INDIA', emailId:'vikas@gmail.com',  password:'123'},
    //   { custId:106, name:'Gopi',   gender:'Male',   country:'INDIA', emailId:'gopi@gmail.com',   password:'123'}
    // ];


  }

  ngOnInit(){
  }

  // getCustomer(custForm: any) {
  //   this.customers.forEach((customer: any) => {
  //     if (customer.custId === custForm.custId) {
  //       this.cust = customer;
  //     }      
  //   });
  // }
  getCustomer(custForm: any) {
    this.service.getCustomerById(custForm.custId).subscribe((custData: any) => {
      this.cust = custData;
      // console.log(data);
    });
  }



}
